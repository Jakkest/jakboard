use core::{convert::Infallible, ops::Deref};

use embedded_hal::digital::v2::InputPin;
use esp32s3_hal::Delay;
use esp_println::println;
use usbd_hid::descriptor::KeyboardReport;

use crate::{debounce::Debounce, key_codes::KeyCode, key_mapping};

#[derive(Clone, Copy)]
pub struct KeyScan<const NUM_ROWS: usize, const NUM_COLS: usize> {
    matrix: [[bool; NUM_ROWS]; NUM_COLS],
}

impl<const NUM_ROWS: usize, const NUM_COLS: usize> Deref for KeyScan<NUM_ROWS, NUM_COLS> {
    type Target = [[bool; NUM_ROWS]; NUM_COLS];

    fn deref(&self) -> &Self::Target {
        &self.matrix
    }
}

impl<const NUM_ROWS: usize, const NUM_COLS: usize> KeyScan<NUM_ROWS, NUM_COLS> {
    pub fn scan(
        rows: &[&dyn InputPin<Error = Infallible>],
        columns: &mut [&mut dyn embedded_hal::digital::v2::OutputPin<Error = Infallible>],
        delay: &mut Delay,
        debounce: &mut Debounce<NUM_ROWS, NUM_COLS>,
    ) -> Self {
        let mut raw_matrix = [[false; NUM_ROWS]; NUM_COLS];

        for (gpio_col, matrix_col) in (0..14).zip(raw_matrix.iter_mut()) {
            if gpio_col & 0b0001 == 0b0001 {
                columns[0].set_high().unwrap();
            } else {
                columns[0].set_low().unwrap();
            }
            if gpio_col & 0b0010 == 0b0010 {
                columns[1].set_high().unwrap();
            } else {
                columns[1].set_low().unwrap();
            }
            if gpio_col & 0b0100 == 0b0100 {
                columns[2].set_high().unwrap();
            } else {
                columns[2].set_low().unwrap();
            }
            if gpio_col & 0b1000 == 0b1000 {
                columns[3].set_high().unwrap();
            } else {
                columns[3].set_low().unwrap();
            }
            delay.delay_micros(50);

            for ((i, gpio_row), matrix_row) in rows.iter().enumerate().zip(matrix_col.iter_mut()) {
                *matrix_row = gpio_row.is_high().unwrap();
                if gpio_row.is_high().unwrap() {
                    println!("{} {}", gpio_col, i);
                }
            }

            columns[0].set_high().unwrap();
            columns[1].set_high().unwrap();
            columns[2].set_high().unwrap();
            columns[3].set_high().unwrap();
            delay.delay_micros(50);
        }

        let matrix = debounce.report_and_tick(&raw_matrix);
        Self { matrix }
    }
}

impl<const NUM_ROWS: usize, const NUM_COLS: usize> From<KeyScan<NUM_ROWS, NUM_COLS>>
    for KeyboardReport
{
    fn from(scan: KeyScan<NUM_ROWS, NUM_COLS>) -> Self {
        let mut keycodes = [0u8; 6];
        let mut keycode_index = 0;
        let mut modifier = 0;

        let mut push_keycode = |key| {
            if keycode_index < keycodes.len() {
                keycodes[keycode_index] = key;
                keycode_index += 1;
            }
        };

        // First scan for any function keys being pressed
        let mut layer_mapping = key_mapping::NORMAL_LAYER_MAPPING;
        for (matrix_column, mapping_column) in scan.matrix.iter().zip(layer_mapping) {
            for (key_pressed, mapping_row) in matrix_column.iter().zip(mapping_column) {
                if mapping_row == KeyCode::Fn && *key_pressed {
                    layer_mapping = key_mapping::FN_LAYER_MAPPING;
                }
            }
        }

        // Second scan to generate the correct keycodes given the activated key map
        for (matrix_column, mapping_column) in scan.matrix.iter().zip(layer_mapping) {
            for (key_pressed, mapping_row) in matrix_column.iter().zip(mapping_column) {
                if *key_pressed {
                    if let Some(bitmask) = mapping_row.modifier_bitmask() {
                        modifier |= bitmask;
                    } else {
                        push_keycode(mapping_row as u8);
                    }
                }
            }
        }

        KeyboardReport {
            modifier,
            reserved: 0,
            leds: 0,
            keycodes,
        }
    }
}