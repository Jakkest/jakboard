#![no_std]
#![no_main]

use usb_device::{bus::{UsbBus as _, UsbBusAllocator}, class::UsbClass};
mod key_scan;
mod key_codes;
mod debounce;
mod hid_descriptor;
mod key_mapping;

use core::{cell::RefCell, convert::Infallible};

use critical_section::Mutex;
use embedded_hal::digital::v2::{InputPin, OutputPin};
use esp32s3_hal::{clock::ClockControl, gpio::{GpioPin, Unknown}, interrupt::{self, Priority}, otg_fs::{UsbBus, USB}, peripherals::{Interrupt, Peripherals}, prelude::*, Delay, IO};
use esp_backtrace as _;
use esp_println::println;
use usb_device::{device::UsbDeviceBuilder, prelude::*};
use usbd_hid::{
    descriptor::KeyboardReport,
    hid_class::{
        HIDClass, HidClassSettings, HidCountryCode, HidProtocol, HidSubClass, ProtocolModeConfig,
    },
};

use crate::{debounce::Debounce, key_scan::KeyScan};

const SCAN_LOOP_RATE_MS: u32 = 1;

const USB_POLL_RATE_MS: u8 = SCAN_LOOP_RATE_MS as u8;

const DEBOUNCE_MS: u8 = 6;

const DEBOUNCE_TICKS: u8 = DEBOUNCE_MS / (SCAN_LOOP_RATE_MS as u8);

const NUM_COLS: usize = 14;
const NUM_ROWS: usize = 6;

const USE_USB: bool = true;

/// The USB Device Driver (shared with the interrupt).

static mut USB_DEVICE: Option<UsbDevice<UsbBus<USB<GpioPin<Unknown, 19>, GpioPin<Unknown, 20>>>>> = None;

/// The USB Bus Driver (shared with the interrupt).
static mut USB_BUS: Option<UsbBusAllocator<UsbBus<USB<GpioPin<Unknown, 19>, GpioPin<Unknown, 20>>>>> = None;

/// The USB Human Interface Device Driver (shared with the interrupt).
static mut USB_HID: Option<HIDClass<UsbBus<USB<GpioPin<Unknown, 19>, GpioPin<Unknown, 20>>>>> = None;

static KEYBOARD_REPORT: Mutex<RefCell<KeyboardReport>> = Mutex::new(RefCell::new(KeyboardReport {
    modifier: 0,
    reserved: 0,
    leds: 0,
    keycodes: [0u8; 6],
}));

static mut EP_MEMORY: [u32; 1024] = [0; 1024];

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();


    let clocks = ClockControl::max(system.clock_control).freeze();
    let mut delay = Delay::new(&clocks);

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let usb = if USE_USB {
        Some(USB::new(peripherals.USB0, io.pins.gpio19, io.pins.gpio20))
    } else {
        None
    };

    let inputs: &[&dyn InputPin<Error = Infallible>] = &[
        &io.pins.gpio5.into_pull_down_input(),
        &io.pins.gpio6.into_pull_down_input(),
        &io.pins.gpio43.into_pull_down_input(),
        &io.pins.gpio44.into_pull_down_input(),
        &io.pins.gpio7.into_pull_down_input(),
        &io.pins.gpio8.into_pull_down_input(),
    ];

    let outputs: &mut [&mut dyn OutputPin<Error = Infallible>] = &mut [
        &mut io.pins.gpio1.into_push_pull_output(),
        &mut io.pins.gpio2.into_push_pull_output(),
        &mut io.pins.gpio3.into_push_pull_output(),
        &mut io.pins.gpio4.into_push_pull_output(),
    ];

    let mut modifier_mask = [[false; NUM_ROWS]; NUM_COLS];

    for (col, mapping_col) in modifier_mask
        .iter_mut()
        .zip(key_mapping::NORMAL_LAYER_MAPPING)
    {
        for (key, mapping_key) in col.iter_mut().zip(mapping_col) {
            *key = mapping_key.is_modifier();
        }
    }

    let mut debounce = Debounce::new(DEBOUNCE_TICKS, modifier_mask);

    let scan = KeyScan::scan(inputs, outputs, &mut delay, &mut debounce);
    critical_section::with(|cs| {
        KEYBOARD_REPORT.replace(cs, scan.into());
    });

    if let Some(usb) = usb {
        let usb_bus = UsbBus::new(usb, unsafe { &mut EP_MEMORY });

        let bus_ref = unsafe {
            USB_BUS = Some(usb_bus);
            USB_BUS.as_ref().unwrap()
        };

        let hid_endpoint = HIDClass::new_with_settings(
            bus_ref,
            hid_descriptor::KEYBOARD_REPORT_DESCRIPTOR,
            USB_POLL_RATE_MS,
            HidClassSettings {
                subclass: HidSubClass::NoSubClass,
                protocol: HidProtocol::Keyboard,
                config: ProtocolModeConfig::ForceReport,
                locale: HidCountryCode::US,
            },
        );

        let keyboard_usb_device = UsbDeviceBuilder::new(bus_ref, UsbVidPid(0x16c0, 0x27db))
            .device_class(0x03)
            .supports_remote_wakeup(true)
            .build();

        unsafe {
            USB_HID = Some(hid_endpoint);
            USB_DEVICE = Some(keyboard_usb_device);
        }
    
        interrupt::enable(Interrupt::USB, Priority::Priority1).unwrap();
    }

    loop {
        let scan = KeyScan::scan(inputs, outputs, &mut delay, &mut debounce);
        critical_section::with(|cs| {
            KEYBOARD_REPORT.replace(cs, scan.into());
        });
        delay.delay_ms(SCAN_LOOP_RATE_MS);
    }
}

#[allow(non_snake_case)]
#[interrupt]
unsafe fn USB() {
    let usb_dev = USB_DEVICE.as_mut().unwrap();
    let usb_hid = USB_HID.as_mut().unwrap();

    if usb_dev.poll(&mut [usb_hid]) {
        usb_hid.poll();
    }

    let report = critical_section::with(|cs| *KEYBOARD_REPORT.borrow_ref(cs));
    if let Err(err) = usb_hid.push_input(&report) {
        match err {
            _ => println!("FUCK"),
        }
    }

    usb_hid.pull_raw_output(&mut [0; 64]).ok();

    if !report_is_empty(&report)
        && usb_dev.state() == UsbDeviceState::Suspend
        && usb_dev.remote_wakeup_enabled()
    {
        usb_dev.bus().resume();
    }
}

fn report_is_empty(report: &KeyboardReport) -> bool {
    report.modifier != 0
        || report.keycodes.iter().any(|key| *key != key_codes::KeyCode::Empty as u8)
}
